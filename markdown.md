## Barcelona

----

Texte amb informació de Barcelona, bonica ciutat del Mediterrani

### Llista ordenada de barris

1. Noubarris
2. Guinardo
3. Trinitat

### Llista desordenada de barris

- Trinitat Nova
- Guineuta
- Ciutat Vella

### Enllaç a la web de [Barcelona](https://www.barcelona.cat/ca/)

### BARCELONA

---

![Imatge barcelona](barcelona.jpg)